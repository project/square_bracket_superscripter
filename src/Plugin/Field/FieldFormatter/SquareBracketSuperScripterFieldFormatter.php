<?php

namespace Drupal\square_bracket_superscripter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'square_bracket_superscripter' formatter.
 *
 * @FieldFormatter(
 *   id = "square_bracket_superscripter",
 *   label = @Translation("Square bracket superscripter"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *   },
 *   edit = {
 *     "editor" = "form"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class SquareBracketSuperScripterFieldFormatter extends TextDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as &$item) {
      $item['#text'] = _square_bracket_superscripter_preprocess_plain_text($item['#text']);
    }
    return $elements;
  }

}
