<?php

namespace Drupal\square_bracket_superscripter\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Square bracket superscripter text filter.
 *
 * @Filter(
 *   id = "filter_square_bracket_superscripter",
 *   title = @Translation("Square bracket superscripter"),
 *   description = @Translation("Converts 123 at the end of next text[123] into superscript!"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterSquareBracketSuperscripter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $new_text = _square_bracket_superscripter_preprocess_plain_text($text);
    return new FilterProcessResult($new_text);
  }

}
