<?php

namespace Drupal\square_bracket_superscripter\TwigExtension;

use Twig\TwigFilter;

/**
 * A test Twig extension that adds a custom function and a custom filter.
 */
class SquareBracketSuperscripterTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('square_bracket_superscripter', [$this, 'squareBracketSuperscripterFilter']),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'square_bracket_superscripter.square_bracket_superscripter_twig_extension';
  }

  /**
   * Helper method to convert square bracketed text to superscript.
   */
  public function squareBracketSuperscripterFilter($text) {
    return _square_bracket_superscripter_preprocess_plain_text($text);
  }

}
